SELECT
  dbo.InforRepresentante($P{TOPAZ_SUCURSAL_SISTEMA}, $P{CODGERENTE}) AS INFORMACION_1,
  (SELECT dbo.NumeroALetrasEntero(PC.No_Escritura) FROM PARAM_CONTRATOS_POR_SUC_2 PC
   WHERE PC.Sucursal = $P{TOPAZ_SUCURSAL_SISTEMA} AND PC.Codigo = $P{CODGERENTE} AND PC.TZ_LOCK = 0) AS INFORMACION_2,
  CAST((SELECT PC.No_Escritura FROM PARAM_CONTRATOS_POR_SUC_2 PC
   WHERE PC.Sucursal = $P{TOPAZ_SUCURSAL_SISTEMA} AND PC.Codigo = $P{CODGERENTE} AND PC.TZ_LOCK = 0) AS VARCHAR(15)) AS INFORMACION_3,
  (SELECT PS.HORA + ' de la ' + PS.TIEMPO  + ' del d�a ' + PS.DIA + ' de ' + PS.Mesletras + ' del ' + PS.anioletras
   FROM PARAM_CONTRATO_SUC PS WHERE PS.IdSucursal = $P{TOPAZ_SUCURSAL_SISTEMA} AND PS.TZ_LOCK = 0) AS INFORMACION_4,
  dbo.InformacionClienteFianza(SL.C5187, 0) AS INFORMACION_5,
  dbo.InformacionClienteFianza(SL.C5000, 1) AS INFORMACION_55,
--+++++++++++++++++++++++++++++++
  (CASE SL.MONEDA
     WHEN 1 THEN dbo.NumeroALetras(SL.C5036)
     WHEN 2 THEN dbo.NumeroALetras(SL.C5036*(SELECT MO.C6440 FROM CO_MONEDAS MO WHERE MO.C6399=3))
     WHEN 3 THEN dbo.NumeroALetras(SL.C5036*(SELECT MO.C6440 FROM CO_MONEDAS MO WHERE MO.C6399=3))
   END) AS INFORMACION_6,
  (CASE SL.MONEDA
     WHEN 1 THEN CAST(SL.C5036 AS VARCHAR(15))
     WHEN 2 THEN CAST((SL.C5036*(SELECT MO.C6440 FROM CO_MONEDAS MO WHERE MO.C6399=3)) AS VARCHAR(15))
     WHEN 3 THEN CAST((SL.C5036*(SELECT MO.C6440 FROM CO_MONEDAS MO WHERE MO.C6399=3)) AS VARCHAR(15))
   END) AS INFORMACION_7,
  (CASE SL.MONEDA
     WHEN 1 THEN dbo.NumeroALetras(SL.C5036/(SELECT MO.C6440 FROM CO_MONEDAS MO WHERE MO.C6399=3))
     WHEN 2 THEN dbo.NumeroALetras(SL.C5036)
     WHEN 3 THEN dbo.NumeroALetras(SL.C5036)
   END) AS INFORMACION_8,
  (CASE SL.MONEDA
     WHEN 1 THEN CAST((SL.C5036/(SELECT MO.C6440 FROM CO_MONEDAS MO WHERE MO.C6399=3)) AS VARCHAR(15))
     WHEN 2 THEN CAST((SL.C5036) AS VARCHAR(15))
     WHEN 3 THEN CAST((SL.C5036) AS VARCHAR(15))
   END) AS INFORMACION_9,
  (SELECT DE.DESTINO
   FROM   PD_RELACIONDESTINO DE, SL_REL_SOL_DESTINO SD
   WHERE  SD.SOLICITUD = SL.C5000
   AND    DE.CODDESTINO = SD.DESTINO
   AND    DE.TIPO='P'
   AND    DE.CODIGO = SL.C5008
   AND    DE.TZ_LOCK = 0
   AND    SD.TZ_LOCK = 0) AS INFORMACION_10,
--+++++++++++++++++++++++++++++++
  (' '+char(10)) AS SALTO,
--+++++++++++++++++++++++++++++++
  (CASE
     WHEN PR.C6259 = 'F'
     THEN ('EL DEUDOR se obliga a pagar la cantidad antes referida en un plazo de ' + dbo.NumeroALetrasEntero(SL.C5035)  +' ('+  CAST(SL.C5035 AS VARCHAR(5)) + ') ' +
           'd�as, contados a partir de esta fecha, por medio de ' + dbo.NumeroALetrasEntero(SL.C5186) + ' (' + cast(SL.C5186 AS VARCHAR(5)) + ') cuotas ' +
           (CASE SL.C5034
              WHEN 1 THEN 'Diaria, '
              WHEN 7 THEN 'Semanal, '
              WHEN 15 THEN 'Quincenal, '
              WHEN 30 THEN 'Mensual, '
              WHEN 60 THEN 'Bi-mensual, '
              WHEN 90 THEN 'Trimestral, '
              WHEN 120 THEN 'Cuatrimestral, '
              WHEN 180 THEN 'Semestral, '
              ELSE ' en 9 meses, '
            END) +
           'consecutivas y nivelada, de principal e intereses seg�n el cronograma de pago adjunto. El primer pago lo efectuar� EL DEUDOR el d�a ' + CONVERT(VARCHAR(12), SL.C5183,113) +
           '. Los pagos se efectuar�n los d�as correspondientes a cada ' +
           (CASE SL.C5034
              WHEN 1 THEN 'D�a, '
              WHEN 7 THEN 'Semana, '
              WHEN 15 THEN 'Quincena, '
              WHEN 30 THEN 'Mes, '
              WHEN 60 THEN 'Bimestre, '
              WHEN 90 THEN 'Trimestre, '
              WHEN 120 THEN 'Cuatrimestre, '
              WHEN 180 THEN 'Semestre, '
              ELSE '9 meses, '
            END) +
           'o el siguiente d�a h�bil, en caso que la fecha de pago cayere en d�a inh�bil conforme a la ley. Los pagos se efectuar�n con sujeci�n al Resumen Informativo y Calendario de Pagos anexo al presente contrato y parte integral del mismo. ')
		 WHEN (PR.C6259 ='C' OR PR.C6259='H') -- Decreciente e irregular
		 THEN
		   ( -- Plan Cuotas Decrecientes y Plan Personalizado (irregular)
		   'EL DEUDOR se obliga a pagar la cantidad antes referida en un plazo de ' + CAST(SL.C5035 AS VARCHAR(5)) +
		   ' d�as, contados a partir de esta fecha, por medio de cuotas y por montos no iguales, de principal e intereses'+
		   ', conforme al Resumen Informativo y Calendario de Pagos anexo a este contrato y parte integral del mismo.'+
		   ' El primer pago lo efectuar� EL DEUDOR el d�a '+ CONVERT(VARCHAR(12), SL.C5183,113) +'. '+
		   'Los siguientes pagos se efectuar�n en la periodicidad convenida los d�as correspondientes a cada mes,'+
		   ' o el siguiente d�a h�bil, en caso que la fecha de pago cayere en d�a inh�bil conforme a la ley,'+
		   ' seg�n se detalla en el Resumen Informativo y Calendario de Pagos anexo al presente contrato y parte integral del mismo'
		   )
		 ELSE
		   ( -- Unico Pago (Al Vencimiento)
		   'EL DEUDOR se obliga a pagar la cantidad antes referida en un plazo ' + CAST(SL.C5035 AS VARCHAR(5)) +
		   ' d�as, contado a partir de esta fecha, por medio de una sola cuota de capital e intereses incluidos de ' + dbo.NumeroALetrasEntero(SL.C5186) +
				(CASE SL.MONEDA   WHEN 1 THEN ' c�rdobas (CS '
								  WHEN 2 THEN ' c�rdobas (CS '
				 ELSE 'd�lares de los Estados Unidos de Am�rica (USD ' END)
		   + cast(SL.C5186 AS VARCHAR(5)) + ')'+
		   ' pagadera al vencimiento del plazo antes se�alado.'
		   ) END ) AS 'INFORMACION_11',
--+++++++++++++++++++++++++++++++
   (dbo.NumeroALetrasEntero(SL.C5039)) AS INFORMACION_12,
   (SUBSTRING(CAST(ROUND(SL.C5039, 2) AS VARCHAR(10)), 1, CHARINDEX('.', CAST(ROUND(SL.C5039, 2) AS VARCHAR(10)))+2)) AS INFORMACION_13,
   --+++++++++++++++++++++++++++++++
   (dbo.NumeroALetras($P{CARGO1})) AS INFORMACION_14,
   (CAST(ROUND($P{CARGO1}, 2) AS VARCHAR(20))) AS INFORMACION_15,
   (dbo.NumeroDecALetras($P{PCARGO1})) AS INFORMACION_16,
   (CAST(ROUND($P{PCARGO1}, 2) AS VARCHAR(20))) AS INFORMACION_17,
   --+++++++++++++++++++++++++++++++
   (dbo.NumeroALetras($P{CARGO2})) AS INFORMACION_18,
   (CAST(ROUND($P{CARGO2}, 2) AS VARCHAR(20))) AS INFORMACION_19,
   (dbo.NumeroDecALetras($P{PCARGO2})) AS INFORMACION_20,
   (CAST(ROUND($P{PCARGO2}, 2) AS VARCHAR(20))) AS INFORMACION_21,
   --+++++++++++++++++++++++++++++++
   (dbo.NumeroALetras($P{CARGO3})) AS INFORMACION_22,
   (CAST(ROUND($P{CARGO3}, 2) AS VARCHAR(20))) AS INFORMACION_23,
   (dbo.NumeroDecALetras($P{PCARGO3})) AS INFORMACION_24,
   (CAST(ROUND($P{PCARGO3}, 2) AS VARCHAR(20))) AS INFORMACION_25,
   --+++++++++++++++++++++++++++++++
   (dbo.NumeroALetras(($P{CARGO4})*SL.C5186)) AS INFORMACION_26,
   (CAST(ROUND(($P{CARGO4})*SL.C5186, 2) AS VARCHAR(20))) AS INFORMACION_27,
   --+++++++++++++++++++++++++++++++
   (SELECT (SELECT (D3.C1216 + ', ' + DI.REFERENCIA)
            FROM   TC_DIRECCIONES DI, TC_DIR_DIM3 D3
            WHERE  DI.ID = CL.C0902 AND DI.FORMATO = 'P' AND DI.TIPODIRECCION = 'R'
            AND    D3.C1212 = DI.DIM1
            AND    D3.C1214 = DI.DIM2
            AND    D3.C1215 = DI.DIM3
            AND    DI.TZ_LOCK = 0
            AND    D3.TZ_LOCK = 0)
    FROM   CL_CLIENTES CL
    WHERE  CL.C0902 = SL.C5187
    AND    CL.TZ_LOCK = 0) AS INFORMACION_28,
   --+++++++++++++++++++++++++++++++
    dbo.InformacionClienteFianza(SL.C5000, 2) AS INFORMACION_29,
   --+++++++++++++++++++++++++++++++
    (SELECT PS.Municipio FROM PARAM_CONTRATO_SUC PS WHERE PS.IdSucursal = $P{TOPAZ_SUCURSAL_SISTEMA} AND PS.TZ_LOCK = 0) AS INFORMACION_30,
    (SELECT CONCAT(CASE
                     WHEN day(PA.FECHAPROCESO)= 1 THEN 'al primer d�a'
                     ELSE CONCAT('a los ',day(PA.FECHAPROCESO),' d�as')
                   END,
                   ' del mes de ',
                   datename(month,PA.FECHAPROCESO),
                   ' del a�o ',
                   year(PA.FECHAPROCESO))
    FROM PARAMETROS PA) AS INFORMACION_31,
   --+++++++++++++++++++++++++++++++
    (SELECT PC.Gerente_Suc
     FROM   PARAM_CONTRATOS_POR_SUC_2 PC
     WHERE  PC.Sucursal = $P{TOPAZ_SUCURSAL_SISTEMA}
     AND    PC.Codigo = $P{CODGERENTE}
     AND    PC.TZ_LOCK = 0) AS INFORMACION_32,
    (SELECT PC.Cedula
     FROM   PARAM_CONTRATOS_POR_SUC_2 PC
     WHERE  PC.Sucursal = $P{TOPAZ_SUCURSAL_SISTEMA}
     AND    PC.Codigo = $P{CODGERENTE}
     AND    PC.TZ_LOCK = 0) AS INFORMACION_33,
    (dbo.FirmasClienteSolidario(SL.C5187, 0)) AS INFORMACION_34,
    (dbo.FirmasClienteSolidario(SL.C5000, 1)) AS INFORMACION_35,
   --+++++++++++++++++++++++++++++++
    (SELECT CL.C1000 + ' Seg�n cedula No ' + RD.NRODOCUMENTO
     FROM   CL_CLIENTES CL, CL_RELPERDOC RD
     WHERE  CL.C0902 = SL.C5187
     AND    RD.IDPERSONA = CL.C0902
     AND    RD.PRINCIPAL = 'S'
     AND    RD.ESTADO = 'A'
     AND    CL.TZ_LOCK = 0
     AND    RD.TZ_LOCK = 0) AS INFORMACION_36,
    (SELECT (DATO.C1000 + ' identificado con cedula No '+ DATO.NRODOCUMENTO)
     FROM (SELECT TOP 1  CL.C1000, RD.NRODOCUMENTO
           FROM   GR_RELACIONGTIACREDSOLIC RG, GR_GARANTIAS GA, CL_CLIENTES CL, CL_RELPERDOC RD
           WHERE  RG.NROSOLICITUD = SL.C5000
           AND    RG.NROGARANTIA = GA.NROGARANTIA
           AND    CL.C0902 = GA.IDPERSONATITULAR
           AND    RD.IDPERSONA = GA.IDPERSONATITULAR
           AND    RD.ESTADO = 'A'
           AND    RD.PRINCIPAL = 'S'
           AND    GA.SUBCLASGTIAS IN (2, 3, 4, 5, 9, 10, 11, 12)
           AND    RG.TZ_LOCK = 0
           AND    GA.TZ_LOCK = 0
           AND    CL.TZ_LOCK = 0
           AND    RD.TZ_LOCK = 0
           GROUP BY CL.C1000, RD.NRODOCUMENTO) AS DATO) AS INFORMACION_37
,'INFORMACION_38' = ( SELECT  CASE WHEN NumeroGarantes>0  THEN InformacionGarante ELSE ''  end   from DBO.Fn_Informacion_Garante_Cn(SL.C5000) )
,'INFORMACION_39' = (SELECT CASE  WHEN (NumeroGarantes =0 AND DEUDOR_SN='SI') THEN 'EL DEUDOR'
												WHEN (NumeroGarantes =1 AND DEUDOR_SN='SI') THEN 'EL DEUDOR Y EL GARANTE PRENDARIO'
												WHEN (NumeroGarantes=2 AND DEUDOR_SN='SI') THEN 'EL DEUDOR Y LOS GARANTES PRENDARIOS'
												WHEN (NumeroGarantes >0 AND DEUDOR_SN='NO') THEN 'EL GARANTE PRENDARIO'
												END  from DBO.Fn_Informacion_Garante_Cn(SL.C5000) )
	-- Parte del seguro (dinamico)
,'INFORMACION_40' = CASE WHEN sl.SEGURO ='S' THEN 'Que EL DEUDOR, a su propio costo y en cumplimiento de la obligaci�n contractual de tomar un seguro de vida y de endosarlo en garant�a a favor de EL ACREEDOR por la cantidad equivalente al monto desembolsado por �ste bajo el presente contrato, decide hacer uso de la facilidad brindada por EL ACREEDOR, adhiri�ndose a la p�liza colectiva de seguro de vida que EL ACREEDOR ha contratado con la instituci�n MAPFRE S.A.,para facilitar el cumplimiento de la presente obligaci�n. Asimismo, EL DEUDOR procede en este acto a endosar en garant�a a favor de EL ACREEDOR la p�liza antes referida, entregando tambi�n los documentos que hacen constar dicho acto. EL DEUDOR se obliga a renovar anualmente, a su propio costo, su afiliaci�n a la presente p�liza y a endosarla en garant�a a favor de EL ACREEDOR mientras subsista cualquier saldo deudor a su cargo bajo el presente contrato. EL DEUDOR confiesa recibir en este acto los documentos relativos a la P�liza de Seguro a la que se adhiri� voluntariamente descrita en el numeral precedente. Esta obligaci�n de entrega de la p�liza, s�lo corre a cargo de EL ACREEDOR cuando EL DEUDOR no hubiere tomado directamente su propia p�liza con su Sociedad de Seguros.'
					ELSE 'Que EL DEUDOR, a su propio costo y en cumplimiento de la obligaci�n contractual de tomar un seguro de vida y de endosarlo en garant�a a favor de EL ACREEDOR, por la cantidad equivalente al monto desembolsado por �ste bajo el presente contrato, presenta a �ste la P�liza de Seguro de Vida que ha tomado directamente con la sociedad de seguros _____________________________. Asimismo, EL DEUDOR procede en este acto a endosar en garant�a a favor de EL ACREEDOR la p�liza antes referida, entregando tambi�n los documentos que hacen constar dicho acto. EL DEUDOR se obliga a renovar anualmente, a su propio costo, la presente p�liza y a endosarla en garant�a a favor de EL ACREEDOR mientras subsista cualquier saldo deudor a su cargo bajo el presente contrato.'
                    END
,'INFORMACION_41'= CASE WHEN SL.SEGURO ='S' THEN 'Que el costo de la p�liza de seguro y de sus sucesivas renovaciones durante la vigencia de este contrato se reflejar�n en el monto de las cuotas que deba pagar EL DEUDOR a EL ACREEDOR.'
				    ELSE 'Que el costo de la p�liza de seguro y de sus sucesivas renovaciones ser�n asumidas y pagada directamente por EL DEUDOR a la Sociedad de Seguros, entendi�ndose que no formar�n parte del presente financiamiento.'
				    END
,'INFORMACION_42'= (SELECT dbo.FirmasGarante(SL.C5000) )
,'INFORMACION_43'= CASE SL.MONEDA WHEN 1 THEN 'c�rdobas'
								  WHEN 2 THEN 'c�rdobas'
								  WHEN 3 THEN 'd�lares de los Estados Unidos de Am�rica'
                   END
,'INFORMACION_44'= CASE SL.MONEDA WHEN 1 THEN 'seg�n tipo de cambio oficial al d�a de hoy establecido por el Banco Central de Nicaragua,'
								  WHEN 2 THEN 'seg�n tipo de cambio oficial al d�a de hoy establecido por el Banco Central de Nicaragua,'
								  WHEN 3 THEN ' '
                   END
,'INFORMACION_45'= CASE SL.MONEDA WHEN 1 THEN 'CS'
								  WHEN 2 THEN 'CS'
								  WHEN 3 THEN 'USD'
                   END
,'INFORMACION_46'= CASE SL.MONEDA WHEN 1 THEN 'equivalente -en d�lares estadounidenses- a la cantidad de'+ (SELECT  dbo.NumeroALetras(SL.C5036/(SELECT MO.C6440 FROM CO_MONEDAS MO WHERE MO.C6399=3)))  +'d�lares de los Estados Unidos de Am�rica (USD '
											   + CAST((SL.C5036) AS VARCHAR(15)) +') '
								  WHEN 2 THEN 'equivalente -en d�lares estadounidenses- a la cantidad de'+ (SELECT  dbo.NumeroALetras(SL.C5036))  +'d�lares de los Estados Unidos de Am�rica (USD '
											   + CAST((SL.C5036/(SELECT MO.C6440 FROM CO_MONEDAS MO WHERE MO.C6399=3)) AS VARCHAR(15)) +') '
								  WHEN 3 THEN ' '
                   END
,'INFORMACION_47'= CASE SL.MODOCOBROCARGOS WHEN 'P' THEN ('en forma diferida y por parte iguales, y se reflejar� en el monto de la(s) cuota(s) a ser pagadas posteriormente por EL DEUDOR')
											ELSE ('al momento del desembolso')
												END


FROM   SL_SOLICITUDCREDITO SL, CO_PRODUCTOS PR
WHERE  SL.C5000 = $P{NROSOLICITUD}*100
AND    SL.C5008 = PR.C6250
AND    SL.TZ_LOCK = 0
AND    PR.TZ_LOCK = 0