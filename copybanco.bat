:: copy.bat hace uso del comando xcopy para copiar los ficheros del repositorio clonado por Jenkins hacia su destino

:: Copia los scripts sql desde el workspace jenkins del proyecto determinado al workspace de flyway
::xcopy C:\Users\nbch\.jenkins\workspace\Clonar-Repo-nbch\SCRIPTS C:\DevOps\flyway-6.5.1\sql /E /Y 

:: Copia los ficheros desde el workspace jenkins del proyecto determinado a la carpeta de Hakuna.
::xcopy "C:\Users\nbch\.jenkins\workspace\Clonar-Repo-nbch\SCRIPTS" "T:\Instalaciones\Desarrollo\NBCH_SQL\hakunamatata\actualizaciones_HM" /E /Y

:: Copia los ficheros QWS desde el workspace jenkins del proyecto determinado a la Biblioteca correspondiente.
:: xcopy "C:\Users\nbch\.jenkins\workspace\Clonar-Repo-nbch\WQS" "T:\Bibliotecas\Testing\BIBLIOTECA_BANCO OBSOLETO\WQS" /E /Y

:: Copia los ficheros OPE desde el workspace jenkins del proyecto determinado a la Biblioteca correspondiente. 
xcopy "C:\Users\nbch\.jenkins\workspace\NBCH_Pruebas_Integrales\TABLAS" "T:\Testing\Jenkins-banco\TABLAS" /E /Y /I

:: Copia los ficheros FML desde el workspace jenkins del proyecto determinado a la Biblioteca correspondiente. 
::xcopy "C:\Users\nbch\.jenkins\workspace\Clonar-Repo-nbch\FML" "T:\Bibliotecas\Testing\BIBLIOTECA_BANCO OBSOLETO\FML" /E /Y

:: Copia los ficheros KETTLE desde el workspace jenkins del proyecto determinado a la carpeta packages correspondiente, se le especifica un origen y un destino.
::xcopy "C:\Users\nbch\.jenkins\workspace\Clonar-Repo-nbch\KETTLE" "T:\Instalaciones\Desarrollo\NBCH_HS\WF10\standalone\userlibrary\default\tools\kettle\packages" /E /Y



